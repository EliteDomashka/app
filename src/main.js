// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.css';
import VueCordova from 'vue-cordova';
import VueHead from 'vue-head';
import VueMoment from 'vue-moment'
import axios from 'axios';

import App from './App';
import router from './router';

Vue.use(Vuetify);
Vue.config.productionTip = false;
Vue.use(VueCordova);
Vue.use(VueHead);
import moment from 'moment'
Vue.prototype.moment = moment
require('moment/locale/uk')

Vue.use(VueMoment, {
  moment
})


// add cordova.js only if serving the app through file://
if (window.location.protocol === 'file:' || window.location.port === '3000') {
  var cordovaScript = document.createElement('script');
  cordovaScript.setAttribute('type', 'text/javascript');
  cordovaScript.setAttribute('src', 'cordova.js');
  document.body.appendChild(cordovaScript);
};

var api = {
  db: null,
  token: null,
  axios: axios,
  callApi(method, data, callback){
    this.axios.get('https://backend-school.in-story.org/api/method/'+method, {
      params: data
    })
      .then(callback)
      .catch(function (error) {
        console.log(error);
      });
  },
  getDB(){
    if (this.db == null){
      this.db = window.sqlitePlugin.openDatabase({
        name: 'my.db',
        location: 'default',
      });
    }
    return this.db;
  },
  updateToken(token){
    this.token = token;
  }
}
var db = null;
document.addEventListener('deviceready', () => {
  db = api.getDB();
  db.transaction(function(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS tasks (task_id NUMERIC, week NUMERIC, day NUMERIC, num NUMERIC, name, task, description)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS notification (id NUMERIC, task_id NUMERIC, week NUMERIC, type NUMERIC)');
  }, function(error) {
    console.log('Transaction ERROR: ' + error.message);
  }, () => {
    document.dispatchEvent(new CustomEvent('initd'));
    console.log('Populated database OK');
  });

  window.FirebasePlugin.getToken((token) => {
    api.updateToken(token);
  }, function(error) {
    console.error(error);
  });
  window.FirebasePlugin.onTokenRefresh((token) => {
    api.updateToken(token);
  }, function(error) {
    console.error(error);
  });
  window.FirebasePlugin.subscribe('tasks');
  window.FirebasePlugin.onNotificationOpen(function(notification) {
    console.log(notification);
    var data = notification;
    if(data['task_id'] != undefined) data['task_id'] = parseInt(data['task_id']);
    if (notification['type'] == 1){
      db.transaction(function(tx) {
        tx.executeSql('INSERT INTO tasks VALUES (?,?,?,?,?,?, ?)', [data['task_id'], parseInt(data['week']), parseInt(data['day']), parseInt(data['num']), data['name'], data['task'], data['description']]);
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
      }, function() {
        console.log('Populated database OK');
      });
    }else if(notification['type'] == 3){
      db.transaction(function(tx) {
        tx.executeSql('DELETE FROM tasks WHERE task_id = ?', [data['task_id']]);
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
      }, function() {
        console.log('Populated database OK');
      });
    }else if (notification['type'] == 2){
      db.transaction(function(tx) {
        tx.executeSql('UPDATE tasks SET task = ?, description = ? WHERE task_id = ?', [data['task'],data['description'], data['task_id']]);
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
      }, function() {
        console.log('Populated database OK');
      });
    }
    if(data['notify_id'] != undefined){
      console.log('insert');
      db.transaction(function(tx) {
        tx.executeSql('INSERT INTO notification VALUES (?,?,?,?)', [parseInt(data['notify_id']), parseInt(data['task_id']), parseInt(data['week']), parseInt(data['type'])]);
        console.error('intsert ok');
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
      }, function() {
        console.log('Populated database OK');
      });
    }
    console.warn(data)
    document.dispatchEvent(new CustomEvent('notify', notification));
    if(data['type'] == "endsync") document.dispatchEvent(new CustomEvent('endsync', notification));


  }, function(error) {
    console.error(error);
  });

});


window.api = api;
window.mainvue = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  head: {
    meta: [
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover',
      },
    ],
  },
});
