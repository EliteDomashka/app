  import Vue from 'vue';
import Router from 'vue-router';
import Settings from '@/components/Settings';
import Main from '@/components/Main';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
    }
  ],
});
